using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenLogin : MonoBehaviour
{
    public GameObject loginPanel;
    
    public void OpenLoginPanel()
    {
        loginPanel.SetActive(true);
    }

    public void CloseLoginPanel()
    {
        loginPanel.SetActive(false);
    }
}
