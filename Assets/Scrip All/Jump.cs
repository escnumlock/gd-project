using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class Jump : MonoBehaviour
{
    private float jumpForce = 17; 
    public float currentTime = 0f;
    private Rigidbody2D _rigidbody2D;
    private bool IsGround = false;
    public int jumpStep = 2;
    [SerializeField] private GameObject bootsIcon;
    [SerializeField] private GameObject timeCount;
    [SerializeField] private Text countdownText;
    public bool doubleJump = false;

    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }
    
    void LateUpdate()
    {

        if (doubleJump == false && IsGround == true && !GameObject.FindWithTag("PauseUI") && !GameObject.FindWithTag("EndUI"))
        {
           ToJump();
           timeCount.SetActive(false);
           bootsIcon.SetActive(false);
        }

        if (doubleJump == true && !GameObject.FindWithTag("PauseUI") && !GameObject.FindWithTag("EndUI"))
        {
            DoubleJump();
            timeCount.SetActive(true);
            bootsIcon.SetActive(true);
            currentTime -= 1 * Time.deltaTime;
            countdownText.text = currentTime.ToString("0");
            if (currentTime <= 0)
            {
                doubleJump = false;
                timeCount.SetActive(false);
                bootsIcon.SetActive(false);
                currentTime = 8;
            }
        }
    }
    
    private void ToJump()
    {
        if (Input.GetKeyDown(KeyCode.Space)) //&& Mathf.Abs(_rigidbody2D.velocity.y) < 0.001f
        {
            _rigidbody2D.AddForce(new Vector2(0, jumpForce ), ForceMode2D.Impulse);
            SoundManager.soundTnstance.PlaySound(0);
        }
    }
    
    private void DoubleJump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && transform.position.y <= 3.5f)//&& jumpStep >= 1 && gameObject.transform.position.y < 3.5f
        {
            _rigidbody2D.AddForce(new Vector2(0, jumpForce ), ForceMode2D.Impulse);
            jumpStep -= 1;
            SoundManager.soundTnstance.PlaySound(0);
            
            if (transform.position.y >= 3.5f)
            {
                _rigidbody2D.AddForce(new Vector2(0, -jumpForce ), ForceMode2D.Impulse);
            }
        }
       
    }
    
    private void OnCollisionEnter2D(Collision2D Coll)
    {
        if (Coll.gameObject.tag == "floor")
        {
            IsGround = true;
        }
    }
    
    private void OnCollisionExit2D(Collision2D other)
    {
        IsGround = false;
    }

    private void OnTriggerEnter2D(Collider2D Coll)
    {
        if (Coll.gameObject.tag == "DoubleJump")
        {
            SoundManager.soundTnstance.PlaySound(1);
            doubleJump = true;
            currentTime = 8;
        }
    }
}
