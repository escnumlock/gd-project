using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnRock : MonoBehaviour
{
    public GameObject prefab1, prefab2, prefab3, prefab4, prefab5;

    public float spawnRate = 1.5f;

    private float nextSpawn = 0f;

    private int whatToSpawn;
    void FixedUpdate()
    {
        if (Time.time > nextSpawn)
        {
            whatToSpawn = Random.Range(1, 6);
            Debug.Log(whatToSpawn);
            switch (whatToSpawn)
            {
                case 1:
                    Instantiate(prefab1, new Vector3(15f,-3.2f), Quaternion.identity);
                    break;
                
                case 2:
                    Instantiate(prefab2, new Vector3(15f,-3.0f), Quaternion.identity);
                    break;
                case 3:
                    Instantiate(prefab3, new Vector3(15f,-2.5f), Quaternion.identity);
                    break;
                case 4:
                    Instantiate(prefab4, new Vector3(15f,-3.2f), Quaternion.identity);
                    break;
                case 5:
                    Instantiate(prefab5, new Vector3(15f,-2.8f), Quaternion.identity);
                    break;
            }

            nextSpawn = Time.time + spawnRate;
        }
    }
}
