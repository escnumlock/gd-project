using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockOBJ : MonoBehaviour
{
    public float rockSpeed = 0;
    
    void FixedUpdate()
    {
        transform.position -= new Vector3(rockSpeed * Time.deltaTime, 0, 0);
        Destroy(gameObject,5f);
        
    }
}
