using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager soundTnstance;
    
    [SerializeField] private AudioSource _audioSource;

    public AudioClip[] soundList; // 0 = jump 1 = item 2 = bgm

    private void Awake()
    {
        soundTnstance = this;
    }

    public void PlaySound(int audioIndexToPlay)
    {
        _audioSource.PlayOneShot(soundList[audioIndexToPlay]);
    }
}
