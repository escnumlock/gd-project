using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public float itemSpeed;
    //[SerializeField] private float timeToDestroy;
    
    void FixedUpdate()
    {
        transform.position -= new Vector3(itemSpeed * Time.deltaTime, 0, 0);
        Destroy(gameObject,10f);
    }
    private void OnTriggerEnter2D(Collider2D Coll)
    {
        if (Coll.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}
