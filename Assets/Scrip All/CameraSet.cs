using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSet : MonoBehaviour
{ 
    void Update()
    {
        transform.position = 
            new Vector3(Mathf.Clamp(transform.position.x, -5.34f, -5.34f),
                        Mathf.Clamp(transform.position.y, -3.67f, 3.67f), 
                        transform.position.z);
    }
}
