using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRock : MonoBehaviour
{
    public GameObject Spawn;

    public GameObject Rock;
   
    void Start()
    {
        InvokeRepeating("a",1,2.5f);
    }

    void a()
    {
        Spawn = Instantiate(Rock,transform.position,Quaternion.identity)as GameObject;
    }
}
