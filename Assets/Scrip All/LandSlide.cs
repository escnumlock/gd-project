using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandSlide : MonoBehaviour
{
    void Update()
    {
        transform.position -= new Vector3(6 * Time.deltaTime, 0, 0);
        if (transform.position.x <= -23)
        {
            transform.position = new Vector3 (23, transform.position.y,0f);
        }
    }
}
