﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenemanager : Score
{
    public GameObject pauseUI;
    public GameObject leaderboardUI;
    public GameObject leaderboardButton;
    public GameObject exitButton;
    public GameObject level1;
    public GameObject level2;

    public GameObject LV1Spawner;

    //public GameObject loginPanel;

    private void Update()
    {
        if (score >= 47)
        {
            LV1Spawner.SetActive(false);
            this.Wait(2f, () =>
            {
                level2.SetActive(true);
                level1.SetActive(false);
            });
        }
    }

    public void startGame()
     {
         this.Wait(0.4f, () =>
         {
             SceneManager.LoadScene("InGame");
             Time.timeScale = 1;
             score = 0;
         });
     }

     public void exitGame()
     {
         this.Wait(0.4f, () => {Application.Quit();});
     }

     public void pauseMenu()
     {
         this.Wait(0.4f, () =>
         {
             pauseUI.SetActive(true);
             Time.timeScale = 0;
         });
     }

     public void contiGame()
     {
         this.Wait(0.4f, () =>
         {
             pauseUI.SetActive(false);
             Time.timeScale = 1;
         });
     }

     public void leaderboard()
     {
         this.Wait(0.4f, () =>
         {
             leaderboardUI.SetActive(true);
             leaderboardButton.SetActive(false);
             exitButton.SetActive(false);
         });
     }

     public void exitleaderboard()
     {
         this.Wait(0.4f, () =>
         {
             leaderboardUI.SetActive(false);
             leaderboardButton.SetActive(true);
             exitButton.SetActive(true);
         });
     }

     public void mainMenu()
     {
         this.Wait(0.4f, () => {SceneManager.LoadScene("MainMenu");});
         //loginPanel.SetActive(false);
         score = 0;
     }
}

