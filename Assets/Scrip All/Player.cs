using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float currentTime = 0f;
    public int useBarr = 0;
    [SerializeField] GameObject barrier;
    public int playerHealth = 1;
    public GameObject endUI;
    public GameObject gameUI;
    [SerializeField] private Text countdownText;
    [SerializeField] private GameObject timeCount;
    [SerializeField] private GameObject shieldIcon;

    private void Update()
    {
        if (playerHealth <= 1)
        {
            barrier.SetActive(false);
            useBarr = 0;
            timeCount.SetActive(false);
            shieldIcon.SetActive(false);
        }

        if (useBarr == 1)
        {
            currentTime -= 1 * Time.deltaTime;
            countdownText.text = currentTime.ToString("0");
            if (currentTime <= 0)
            {
                barrier.SetActive(false);
                currentTime = 8;
                playerHealth = 1;
                useBarr = 0;
                timeCount.SetActive(false);
                shieldIcon.SetActive(false);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D Coll)
    {
        if (Coll.gameObject.tag == "Rock")
        {
            Debug.Log("Collision!!!!!!!");
            playerHealth -= 1;
            if (playerHealth <= 0)
            {
                Time.timeScale = 0;
                GameOver();
            }
        } 
    }

    private void OnTriggerExit2D(Collider2D Coll)
    {
        if (Coll.gameObject.tag == "Rock")
        {
            playerHealth -= 1;
            barrier.SetActive(false);
            timeCount.SetActive(false);
            shieldIcon.SetActive(false);
        }
    }

    public void GameOver()
    {
        endUI.SetActive(true);
        gameUI.SetActive(false);
    }
    
    public void OnTriggerEnter2D(Collider2D Coll)
    {
        if (Coll.gameObject.tag == "Shield")
        {
            SoundManager.soundTnstance.PlaySound(1);
            Debug.Log("Shield!!!!!!!");
            playerHealth = 2;
            barrier.SetActive(true);
            useBarr = 1;
            currentTime = 8;
            timeCount.SetActive(true);
            shieldIcon.SetActive(true);
        }
    }
}
