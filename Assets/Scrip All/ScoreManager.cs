using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : Score
{
    public Text ScoreText;

    void Update()
    {
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            PlayerPrefs.SetFloat("score",score);
            score += 1 * Time.deltaTime * 2;
            ScoreText.text = "Score:" + ((int) score).ToString();
        }
    }
}
