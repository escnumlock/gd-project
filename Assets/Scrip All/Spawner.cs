using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<GameObject> objectToSpawn = new List<GameObject>();

    //public GameObject spawnToObject;

    public float timeToSpawn;

    private float currentTimeToSpawn;

    public bool isTimer;

    public bool isRandomized;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isTimer)
        {
            //UpDateTimer();
        }
        
        if (currentTimeToSpawn > 0)
        {
            currentTimeToSpawn -= Time.deltaTime;
        }
        else
        {
            SpawnObject();
            currentTimeToSpawn = timeToSpawn;
        }
    }

    public void SpawnObject()
    {
        //Instantiate(objectToSpawn, spawnToObject.transform);
        int index = isRandomized ? Random.Range(0, objectToSpawn.Count) : 0;
        if (objectToSpawn.Count > 0)
        {
            Instantiate(objectToSpawn[index], transform.position, transform.rotation);
        }
        
    }
}
