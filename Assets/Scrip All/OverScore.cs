using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OverScore : MonoBehaviour
{
    public Text overScore;

    public float endScore;
    void Start()
    {
        endScore = PlayerPrefs.GetFloat("score", endScore);
    }
    
    void Update()
    {
        overScore.text = "" + ((int) endScore).ToString();
    }
   
}
