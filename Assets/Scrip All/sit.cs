using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sit : MonoBehaviour
{
    private float sitForce = 15;
    private Rigidbody2D _rigidbody2D;
    public Animator animator;
    private BoxCollider2D collider;
    private Vector2 originSize;
    
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        collider = GetComponent<BoxCollider2D>();
        originSize = collider.size;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            animator.SetBool("sit",true);
            collider.size = new Vector2(collider.size.x, 2.8f);
            if (gameObject.transform.position.y > -2)
            {
                _rigidbody2D.AddForce(new Vector2(0, -sitForce ), ForceMode2D.Impulse);
            }
        }
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            animator.SetBool("sit",false);
            collider.size = originSize;
        }
    }
}
