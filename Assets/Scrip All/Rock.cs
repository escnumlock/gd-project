using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : Barrier
{
    public Collider2D coll;
    public float rockSpeed = 0;

    private void Start()
    {
        coll = GetComponent<Collider2D>();
    }

    void FixedUpdate()
    {
        transform.position -= new Vector3(rockSpeed * Time.deltaTime, 0, 0);
        Destroy(gameObject,5f);
        
        if (findBarr == 1)
        {
            coll.isTrigger = true;
        }

        if (findBarr == 0)
        {
            coll.isTrigger = false;
        }
    }
}

