using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SpaceMan : MonoBehaviour
{
    public bool IJ;

    public bool IsGrounded;
    
    void Start()
    {
        IJ = false;
    }
    
    void Update()
    {
        if (IsGrounded == true)
        {

            if (Input.GetKeyDown(KeyCode.Space) && IJ == false)
            {
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector3(0, 14, 0);
                    IJ = true;
                }
            }
        }

        if (IJ == true)
        {
            IJ = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D Coll)
    {
        if (Coll.gameObject.tag=="floor")
        {
            IsGrounded = true;
        }
        
        if (Coll.gameObject.tag=="Finish")
        {
            IJ = false;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        IsGrounded = false;
    }
    
}
